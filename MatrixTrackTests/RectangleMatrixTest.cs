using Microsoft.VisualStudio.TestTools.UnitTesting;
using MatrixTrack;

namespace MatrixTrackTests
{
    [TestClass]
    public class RectangleMatrixTest
    {
        [TestMethod]
        public void GetMatrixTrackTest()
        {
            IMatrixBuilder matrixBuilder = new RectangleMatrixBuilder();
            IMatrix matrix = matrixBuilder.CreateMatrix((3, 3), 2, 2);

            Assert.AreEqual(matrix.GetMatrixTrack(), 6);
        }
    }
}
