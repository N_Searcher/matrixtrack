﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MatrixTrack;

namespace MatrixTrackTests
{
    [TestClass]
    public class RectangleMatrixBuilderTest
    {
        [TestMethod]
        public void RectMatrixBuilderMatrixSizeLessThatZero()
        {
            IMatrixBuilder matrixBuilder = new RectangleMatrixBuilder();

            try
            {
                IMatrix matrix = matrixBuilder.CreateMatrix((-2, 3));
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "Columns and rows values are 0 or less!");
            }
        }

        [TestMethod]
        public void RectMatrixBuilderCreateMatrix()
        {
            IMatrixBuilder matrixBuilder = new RectangleMatrixBuilder();
            IMatrix matrix = matrixBuilder.CreateMatrix((3, 3));

            Assert.AreEqual(matrix.ColsQuantity, 3);
            Assert.AreEqual(matrix.RowsQuantity, 3);
        }
    }
}
