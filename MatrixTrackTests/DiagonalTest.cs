﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MatrixTrack;

namespace MatrixTrackTests
{
    [TestClass]
    public class DiagonalTest
    {
        [TestMethod]
        public void DiagonalSizeTest()
        {
            Diagonal diagonal = new Diagonal(new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }, (0, 0), (1, 1));

            Assert.AreEqual(diagonal.Length, 3);
        }

        [TestMethod]
        public void DiagonalNumbersTest()
        {
            Diagonal diagonal = new Diagonal(new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }, (0, 0), (1, 1));

            Assert.AreEqual(diagonal[0].value, 1);
        }
    }
}
