﻿using System;

namespace MatrixTrack
{
    class Program
    {
        static void Main(string[] args)
        {
            TheMatrixShow();
        }

        public static void TheMatrixShow()
        {
            IMatrixBuilder matrixBuilder = new RectangleMatrixBuilder();
            MatrixIO matrixIO = new MatrixIO();
            (int, int)matrixSize = matrixIO.GetMatrixSize();
            IMatrix matrix;

            try
            {
                matrix = matrixBuilder.CreateMatrix(matrixSize, 0, 100);
                matrixIO.PrintMatrixData(matrix);
                matrixIO.PrintMatrixData(matrix.GetMatrixTrack());
            }
            catch (Exception error)
            {
                matrixSize = (0, 0);
                matrixIO.PrintMatrixError(error.Message);
                TheMatrixShow();
            }
        }
    }
}
