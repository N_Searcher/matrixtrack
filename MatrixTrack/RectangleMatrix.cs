﻿using System.Linq;

namespace MatrixTrack
{
    public class RectangleMatrix : IMatrix
    {
        private int[,] matrixNumbersSet;

        public RectangleMatrix(int[,] matrixNumbers)
        {
            matrixNumbersSet = matrixNumbers;
            MainDiagonal = new Diagonal(matrixNumbersSet, (0, 0), (1, 1));
        }

        public int this[int row, int column]
        {
            get
            {
                return matrixNumbersSet[row, column];
            }

            set
            {
                matrixNumbersSet[row, column] = value;
            }
        }

        public int RowsQuantity
        {
            get => matrixNumbersSet.GetLength(0);
        }

        public int ColsQuantity
        {
            get => matrixNumbersSet.GetLength(1);
        }

        public Diagonal MainDiagonal 
        { 
            get; 
            set; 
        }

        public int GetMatrixTrack()
        {
            return MainDiagonal.GetDiagonalNumbersSum();
        }
    }
}
