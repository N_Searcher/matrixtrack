﻿using System;

namespace MatrixTrack
{
    public class RectangleMatrixBuilder : IMatrixBuilder
    {
        int[,] CreateRandomNumbers(int rows, int cols, int minNumber, int maxNumber)
        {
            int[,] randomValues = new int[rows, cols];
            Random rand = new Random();

            for (int i = 0; i < rows; i++)
            {
                for (int u = 0; u < cols; u++)
                {
                    randomValues[i, u] = rand.Next(minNumber, maxNumber + 1);
                }
            }

            return randomValues;
        }

        public IMatrix CreateMatrix((int rows, int cols) matrixSize, int minNumber = int.MinValue, int maxNumber = int.MaxValue)
        {
            if (matrixSize.rows > 0 && matrixSize.cols > 0)
            {
                return new RectangleMatrix(CreateRandomNumbers(matrixSize.rows, matrixSize.cols, minNumber, maxNumber));
            }

            throw new Exception("Columns or rows values are 0 or less!");
        }
    }
}
