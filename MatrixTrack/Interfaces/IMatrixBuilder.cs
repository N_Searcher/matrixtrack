﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixTrack
{
    public interface IMatrixBuilder
    {
        IMatrix CreateMatrix((int rows, int cols) matrixSize, int minNumber = int.MinValue, int maxNumber = int.MaxValue);
    }
}
