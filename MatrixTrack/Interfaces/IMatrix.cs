﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrixTrack
{
    public interface IMatrix
    {
        int this[int rows, int columns]
        {
            get;
            set;
        }

        int RowsQuantity { get; }
        int ColsQuantity { get; }

        Diagonal MainDiagonal
        {
            get;
            set;
        }

        int GetMatrixTrack();
    }
}
