﻿using System.Linq;

namespace MatrixTrack
{
    public class Diagonal
    {
        private (int row, int col, int value)[] DiagonalNumbers
        {
            get;
            set;
        }

        void GetNumbersSubSet(int[,] matrixNumbers, (int row, int col) startPoint, (int row, int col) offset)
        {
            for (int i = startPoint.row, u = startPoint.col; i < DiagonalNumbers.Length; i += offset.row, u += offset.col)
            {
                DiagonalNumbers[i] = (i, u, matrixNumbers[i, u]);
            }
        }

        int GetDiagonalLength(int rowsQuantity, int colsQuantity)
        {
            return colsQuantity > rowsQuantity ? rowsQuantity : colsQuantity;
        }

        public (int row, int col, int value) this[int index]
        {
            get
            {
                return DiagonalNumbers[index];
            }
        }

        public Diagonal(int[,] matrixNumbers, (int row, int col) startPoint, (int row, int col) offset)
        {
            if (matrixNumbers != null)
            {
                int length = GetDiagonalLength(matrixNumbers.GetLength(0), matrixNumbers.GetLength(1));

                DiagonalNumbers = new (int row, int col, int value)[length];
                GetNumbersSubSet(matrixNumbers, startPoint, offset);
            }
        }

        public int GetDiagonalNumbersSum()
        {
            return DiagonalNumbers.Select(x=>x.value).Sum();
        }

        public int Length
        {
            get
            {
                return DiagonalNumbers.Length;
            }
        }
    }
}
