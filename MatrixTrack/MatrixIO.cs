﻿using System;
namespace MatrixTrack
{
    class MatrixIO
    {
        public (int, int) GetMatrixSize()
        {
            (int rows, int cols) matrixSize = (0, 0);

            matrixSize.rows = ReadMatrixSize("rows");
            matrixSize.cols = ReadMatrixSize("cols");

            return matrixSize;
        }

        public void PrintMatrixData(IMatrix matrix)
        {
            Console.WriteLine();

            int mainDiagonalIndex = 0;

            for (int i = 0; i < matrix.RowsQuantity; i++)
            {
                for (int u = 0; u < matrix.ColsQuantity; u++)
                {
                    Console.ResetColor();

                    if (mainDiagonalIndex < matrix.MainDiagonal.Length && i == matrix.MainDiagonal[mainDiagonalIndex].row && u == matrix.MainDiagonal[mainDiagonalIndex].col)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        mainDiagonalIndex++;
                    }

                    Console.Write(matrix[i, u] + "\t");
                }
                Console.Write("\r\n");
            }

            Console.WriteLine();
        }

        public void PrintMatrixData(int track)
        {
            Console.WriteLine($"Matrix Track is {track}");
        }

        public void PrintMatrixError(string errorMessage)
        {
            Console.WriteLine(errorMessage);
        }

        int ReadMatrixSize(string sizeName)
        {
            int size;
            Console.Write($"Paste {sizeName} quantity: ");
            int.TryParse(Console.ReadLine(), out size);

            return size;
        }
    }
}
